from django.shortcuts import render, redirect, get_object_or_404
from todos.models import TodoList, TodoItem
from todos.forms import ListForm, ItemForm


# Create your views here.


def todo_list_list(request):
    todo_list = TodoList.objects.all
    context = {
        "todo_lists": todo_list
    }
    return render(request, "todos/list.html", context)


def todo_list_detail(request, id):
    todo = TodoList.objects.get(id=id)
    context = {
        "todo_object": todo
    }
    return render(request, "todos/detail.html", context)


def create_list(request):
    if request.method == "POST":
        form = ListForm(request.POST)
        if form.is_valid():
            list = form.save()
            return redirect("todo_list_detail", id=list.id)
    else:
        form = ListForm()
    context = {
        "form": form,
    }
    return render(request, "todos/create.html", context)


def update_list(request, id):
    list = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = ListForm(request.POST, instance=list)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", list.id)
    else:
        form = ListForm()
    context = {
        "form": form,
    }
    return render(request, "todos/update.html", context)

def delete_list(request, id):
    list = TodoList.objects.get(id=id)
    if request.method == "POST":

        list.delete()
        return redirect("todo_list_list")
    return render(request, "todos/delete.html")


def create_item(request):
    if request.method == "POST":
        form = ItemForm(request.POST)
        if form.is_valid():
            create = form.save()
            return redirect("todo_list_detail", create.list.id)
    else:
        form = ItemForm()
    context = {
        "new_item": form,
    }
    return render(request, "todos/create_item.html", context)


def edit_item(request, id):
    edit = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = ItemForm(request.POST, instance=edit)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", edit.list.id)
    else:
        form = ItemForm()
    context = {"update_item": form}
    return render(request, "todos/edit_item.html", context)
